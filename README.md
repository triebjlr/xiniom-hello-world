# xiniom-hello-world

## Getting started

`$ npm install xiniom-hello-world --save`

### Mostly automatic installation

`$ react-native link xiniom-hello-world`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `xiniom-hello-world` and add `XiniomHelloWorld.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libXiniomHelloWorld.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.xiniom.hello.world.XiniomHelloWorldPackage;` to the imports at the top of the file
  - Add `new XiniomHelloWorldPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':xiniom-hello-world'
  	project(':xiniom-hello-world').projectDir = new File(rootProject.projectDir, 	'../node_modules/xiniom-hello-world/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':xiniom-hello-world')
  	```


## Usage
```javascript
import XiniomHelloWorld from 'xiniom-hello-world';

// TODO: What to do with the module?
XiniomHelloWorld;
```
